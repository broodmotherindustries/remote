import { NgModule } from '@angular/core';
import { SharedComponent } from './shared.component';
import { JoystickComponent } from './joystick/joystick.component';

@NgModule({
  declarations: [SharedComponent, JoystickComponent],
  imports: [
  ],
  exports: [SharedComponent, JoystickComponent]
})
export class SharedModule { }
