import { Component, OnInit, Output, EventEmitter, Input, AfterViewInit } from "@angular/core";
import Manager from "nipplejs";

@Component({
  selector: 'lib-joystick',
  templateUrl: './joystick.component.html',
  styleUrls: ['./joystick.component.css']
})
export class JoystickComponent implements OnInit, AfterViewInit {
  constructor() {}
  manger = null;
  @Input() isHorizontalLocked: boolean = false;
  @Input() isVertialLocked: boolean = false;
  @Input() Id: string = 'zone_joystick';
  @Output() joystickDir: EventEmitter<string> = new EventEmitter();
  @Output() joystickMoved: EventEmitter<any> = new EventEmitter();

  ngAfterViewInit(){
    let elm = document.getElementById(this.Id)

    console.log(elm);
    const joystick = Manager.create({
      zone: elm,
      mode: 'static',
      position: { left: '50%', top: '50%' },
      color: 'grey',
      lockX: this.isHorizontalLocked,
      lockY: this.isVertialLocked
    });

    joystick.on('dir', (evt, nipple) => {
      this.joystickDir.emit(nipple.direction.angle);
    });
    joystick.on('move', (evt, nipple) => {
      this.joystickMoved.emit(nipple);
    })
  }

  ngOnInit() {
  }
}
