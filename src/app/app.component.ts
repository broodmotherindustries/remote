import { Component,  OnInit, AfterViewInit } from "@angular/core";
import { SocketService } from "./services/socket.service";
import { Message } from "./model/message";
import { Action } from "./model/action";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, AfterViewInit {
  private messages: Message[] = [];
  private ioConnection: any;
  context = null;
  canvas = null;
  fps;
  fpsInterval;
  startTime;
  now;
  then;
  elapsed;
  frames = '';
  frameCount =0;
  logs ='';
  constructor(private socketService: SocketService) {}

  public ngOnInit() {
    this.initConnection();
  }

  private initConnection(): void {
    this.socketService.initSocket();
    this.ioConnection = this.socketService
      .onMessage()
      .subscribe((message: Message) => {
        this.messages.push(message);
      });

    this.socketService.onMessage().subscribe(data => {
      console.log(data);
    });
  }

  onJoystickMoved(direction: any) {
    console.log(direction);
    this.logs += direction + '<br/>';
    let message: Message = { content: direction.direction.angle, action: Action.MOVED };
    this.socketService.send(message);
  }

  ngAfterViewInit(){
    this.canvas = document.getElementById('piCam');
    this.context = this.canvas.getContext("2d");
    this.startAnimating(30);
  }

  startAnimating(fps) {
    this.fpsInterval = 1000 / fps;
    this.then = Date.now();
    this.startTime = this.then;
    console.log(this.startTime);
    this.animate();
}

  public animate(){
    requestAnimationFrame(() => {
      this.now = Date.now();
      this.elapsed = this.now - this.then;
      this.animate();
      if (this.elapsed > this.fpsInterval) {
        this.draw();
        // Get ready for next frame by setting then=now, but...
        // Also, adjust for fpsInterval not being multiple of 16.67
        this.then = this.now - (this.elapsed % this.fpsInterval);
        let sinceStart = this.now - this.startTime;
        var currentFps = Math.round(1000 / (sinceStart / ++this.frameCount) * 100) / 100;
        this.frames = Math.round(sinceStart / 1000 * 100) / 100 + " secs @ " + currentFps + " fps.";
      }

    });
  }

  public draw(){
    let piImage = new Image();
    piImage.onload = (event) => {
      this.context.drawImage(piImage, 0, 0, this.canvas.width, this.canvas.height);
    }
    piImage.src = "http://192.168.0.28/picam/cam_pic.php?time=" + new Date().getTime();
  }

}
