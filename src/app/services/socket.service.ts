import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Observer } from "rxjs";
import { Message } from "../model/message";
import { Event } from "../model/event";
import * as socketIo from "socket.io-client";
import { Action } from '../model/action';

const SERVER_URL = "ws://localhost:5001/";

@Injectable()
export class SocketService {
  private socket;

  public initSocket(): void {
    this.socket = socketIo(SERVER_URL);

  }

  public send(message: Message): void {
    this.socket.emit(Action[message.action], message);
  }

  public onMessage(): Observable<Message> {
    return new Observable<Message>(observer => {
      this.socket.on("message", (data: Message) => observer.next(data));
    });
  }

  public onEvent(event: Event): Observable<any> {
    return new Observable<any>(observer => {
      this.socket.on(event, (data: string) => {
        if (data) {
          observer.next(JSON.parse(data));
        } else {
          observer.next();
        }
      });
    });
  }

}
