export enum Event {
    CONNECT = 'connect',
    DISCONNECT = 'disconnect',
    NEWBOT = 'newbot',
    UPDATEBOT ='updateBot'
}